##Static menu
Add menu command: "Objects", "Praat", "-", "", 0, ""
Add menu command: "Objects", "Praat", "Finder", "", 0, ""

Add menu command: "Objects", "Praat", "Run Finder...", "Finder", 1, "scripts/finder.praat"
Add menu command: "Objects", "Praat", "-", "Finder", 1, ""

Add menu command: "Objects", "Praat", "Index", "Finder", 1, ""
Add menu command: "Objects", "Praat", "Create index by tier number...", "Index", 2, "scripts/index_by_tier_number.praat"
Add menu command: "Objects", "Praat", "Create index by tier name...", "Index", 2, "scripts/index_by_tier_name.praat"
Add menu command: "Objects", "Praat", "-", "Index", 2, ""
Add menu command: "Objects", "Praat", "Import...", "Index", 2, "scripts/set_import_index.praat"
Add menu command: "Objects", "Praat", "Export...", "Index", 2, "scripts/set_export_index.praat"

Add menu command: "Objects", "Praat", "-", "Finder", 1, ""
Add menu command: "Objects", "Praat", "About...", "Tokenizer", 1, "./scripts/about.praat"