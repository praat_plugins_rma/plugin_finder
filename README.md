# Finder

A Praat plug-in which finds and displays one-by-one a set of interval labels in the TextGridEditor.

For more information, you can visit the Wiki of this project.