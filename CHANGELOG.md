# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unrealised]

## [1.0.0]

### Added
  - Run Finder...
  - Index menu
    - `Create index...` commands
    - `Import...` command
    - `Export...` command
  - Settings menu
    - `Set working directories...`
  -  `About` command

