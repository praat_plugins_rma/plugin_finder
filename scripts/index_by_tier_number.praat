# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc

@config.init: "../preferences/preferences.txt"

tg_folder$ = config.init.return$["textgrids_dir"]
tier = number(config.init.return$["tier_number"])
match_pattern$ = config.init.return$["match_pattern"]

beginPause: "index"
  sentence: "Textgrid folder", tg_folder$
  natural: "Tier", tier
  sentence: "Match_pattern", match_pattern$
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "textgrids_dir", textgrid_folder$
@config.setField: "tier", string$(tier)
@config.setField: "match_pattern", match_pattern$

index = Create Table with column names: "index", 0, "text tmin tmax filename path tier"
fileList = Create Strings as file list: "fileList", textgrid_folder$ + "/*.TextGrid"

selectObject: fileList
n = Get number of strings

for i to n
  fileName$ = object$[fileList, i]
  tgDir$ = textgrid_folder$ + "/" + fileName$

  tgID = Read from file: tgDir$
  nTiers = Get number of tiers
  if tier <= nTiers
    nIntervals = Get number of intervals: tier
    for interval to nIntervals
      selectObject: tgID
      text$ = Get label of interval: tier, interval

      # if match_pattern$ matches the interval text, then add interval info to the index
      if index_regex(text$, match_pattern$)
        tmin = Get start time of interval: tier, interval
        tmax = Get end time of interval: tier, interval

        # Add data to the index table
        selectObject: index
        Append row
        row = Object_'index'.nrow
        Set string value: row, "text", text$
        Set string value: row, "filename", fileName$
        Set string value: row, "path", textgrid_folder$
        Set numeric value: row, "tmin", tmin
        Set numeric value: row, "tmax", tmax
        Set numeric value: row, "tier", tier
      endif
    endfor
  endif
  removeObject: tgID
endfor
removeObject: fileList

if Object_'index'.nrow
  @config.setField: "finder.row", "1"
  selectObject: index
  Save as text file: "../preferences/index.Table"
else
  selectObject: index
  deleteFile: "../preferences/index.Table"
  pauseScript: "The index is empty. Try again."
endif