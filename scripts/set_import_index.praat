include ../../plugin_finder/procedures/config.proc

@config.init: "../../plugin_finder/preferences/preferences.txt"

indexDir$ = "../../plugin_finder/preferences/index.Table"
deleteFile(indexDir$)

indexDir_choosed$ = chooseReadFile$: "Import Index from"

if indexDir_choosed$ <> ""
  tb = Read Table from tab-separated file: indexDir_choosed$

  colName$[1] = "filename"
  colName$[2] = "text"
  colName$[3] = "tmin"
  colName$[4] = "tmax"

  for i to 4
    colIndex = Get column index: colName$[i]
    i = if colIndex = 0 then 4 else i fi
  endfor
  
  if !colIndex
    beginPause: "import"
      comment: "Set the following fields"
      optionMenu: "Filename", 1
        @option.DisplayRowNames: tb
      optionMenu: "Text", 1
        @option.DisplayRowNames: tb
      optionMenu: "Tmin", 1
        @option.DisplayRowNames: tb
      optionMenu: "Tmax", 1
        @option.DisplayRowNames: tb
    clicked = endPause: "Continue", "Quit", 1
    if clicked = 2
      exitScript()
    endif
    Set column label (label): filename$, colName$[1] 
    Set column label (label): text$, colName$[2]
    Set column label (label): tmin$, colName$[3]
    Set column label (label): tmax$, colName$[4]
  endif

  Save as text file: indexDir$
  @config.setField: "case", "1"
endif

procedure option.DisplayRowNames: .tb
    option: "Choose a column name"
    for .i to Object_'.tb'.ncol
      option: Object_'.tb'.col$[.i]
    endfor
endproc
