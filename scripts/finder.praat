# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc

@config.init: "../preferences/preferences.txt"

beginPause: "index"
  comment: "Input:"
  comment: "The directories where your files are stored..."
  sentence: "Audio folder", config.init.return$["sounds_dir"]
  sentence: "Textgrid folder", config.init.return$["textgrids_dir"]
  comment: "Output:"
  comment: "Display settings..."
  real: "Margin", number(config.init.return$["finder.margin"])
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "textgrids_dir", textgrid_folder$
@config.setField: "sounds_dir", audio_folder$
@config.setField: "finder.margin", string$(margin)

indexDir$ = "../preferences/index.Table"
if !fileReadable(indexDir$)
  pauseScript: "finder: Create an index first"
endif

index = Read from file: indexDir$
nrow = Object_'index'.nrow
if !nrow
  exitScript: "No files in the index"
endif

row = number(config.init.return$["finder.row"])
while 1
  row = if row > nrow then 1 else row fi

  #Get info from index
  text$ = object$[index, row, "text"]
  tgName$ = object$[index, row, "filename"]
  sdName$ = tgName$ - "TextGrid" + "wav"
  sdDir$ = audio_folder$ + "/" + sdName$
  tgDir$ = textgrid_folder$ + "/" + tgName$

  tmin = object[index, row, "tmin"]
  tmax = object[index, row, "tmax"]
  tmid = (tmax - tmin)*0.5 + tmin
  tier = object[index, row, "tier"]

  #Display
  if fileReadable(tgDir$) and fileReadable(sdDir$)
    sd = Open long sound file: sdDir$
    tg = Read from file: tgDir$
  elsif fileReadable(tgDir$)
    tg = Read from file: tgDir$
    sd = Create Sound from formula: "sineWithNoise", 1, 0, 1, 44100, "1"
    plusObject: tg
    Clone time domain
  endif

  selectObject: sd
  plusObject: tg
  View & Edit
  editor: tg
  for i to tier - 1
    Select next tier
  endfor
  
  Select: tmin - margin, tmax + margin
  Zoom to selection
  Move cursor to: tmid

  beginPause: "finder"
    comment: "Case: 'row'/'nrow'"
    comment: "Text: " + if length(text$)> 25 then left$(text$, 25) + "..." else text$ fi
    natural: "Next case",  if (row + 1) > nrow then 1 else row + 1 fi
  clicked = endPause: "Continue", "Save", "Quit", 1
  endeditor

  if clicked = 2
    selectObject: tg
    Save as text file: tgDir$
  endif
  removeObject: sd, tg
  @config.setField: "finder.row", string$(row)
  row = next_case

  if clicked = 3
    removeObject: index
    exitScript()
  endif
endwhile